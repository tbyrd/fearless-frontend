window.addEventListener('DOMContentLoaded', async () => {
    //Requests Data from the API
    const url = 'http://localhost:8000/api/locations/'

    try {
        const response = await fetch(url);  //Tries to fetch the data from the API via the url

        if (!response.ok) { //If the data is not fetched, returns a error message
            console.error("there has been an error") //Error Message returned
        } else { //If the response is okay
            const data = await response.json(); //Waits for the promise, then it converts the Json Data and parses it into a javascript object (dictionary)
            const selectTag = document.getElementById('location'); //Gets the value associated with the "location" and associates it with the location select tag in the HTML

            for (let location of data.locations) { //Iterates through the locations saved in the data variable
                let option = document.createElement('option'); //Allows us to create an option element with our javascript code (option = tag name)
                option.value = location.id; //Gets the location id and saves it in the option.value variable
                option.innerHTML = location.name; //Gets the location name and saves it in the innerHTML
                selectTag.appendChild(option) //Adds the location element to the dropdown menu
            }

            const formTag = document.getElementById('create-conference-form'); //Gets the form data and saves it in the formTag variable
            formTag.addEventListener('submit', async event => { //Accesses the formTag element, then listens for submit
                event.preventDefault(); //Prevents the data entered from saving
                const formData = new FormData(formTag); //Gets the data submitted in the form, saves it to the formData variable
                const json = JSON.stringify(Object.fromEntries(formData)); //Converts the data to a Json string
                const conferenceUrl = 'http://localhost:8000/api/conferences/'; //Saves the API url in conferenceURL
                const fetchConfig = { //
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig); //Waits for the conferenceURL and fetchConfig, saves it in the response variable
                if (response.ok) { //If the response is okay, then it resets the form, so its blank onces submitted
                    formTag.reset(); //Resets the form
                    const newConference = await response.json()
                }
            });
        }
    } catch (e) { //e = event handler
        console.error("There has been an error") //Displays a message, there will be an error
    }
});
