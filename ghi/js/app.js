function createCard(name, description, pictureUrl, start, end, location) {
    return `
            <div>
                <div class="shadow p-3 mb-5 bg-body rounded">
                    <div class="p-2 bg-light border">
                        <img src="${pictureUrl}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">${name}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">${location}</h5>
                            <p class="card-text">${description}</p>
                    </div>
                     <div class="card-footer">
                        ${start} - ${end}
                    </div>
                </div>
            </div>
`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            console.log("You have a bad request");
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    //const detailTag = document.querySelector('.card-text');
                    //detailTag = details.conference.description;
                    const title = details.conference.name;
                    //const titleTag = document.querySelector('.card-title');
                    //titleTag = details.conference.description;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const start = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`
                    const endDate = new Date(details.conference.ends);
                    const end = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, start, end, location);
                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.error(e);
        const html = errorAlert(e);
        const error = document.querySelector('.col');
        error.innerHTML += html;
    }

});

{/* <div class="container text-center">
<div class="column">
<div class="card">
  <div class="col">
  <div class="d-grid gap-3">
  <div class="shadow p-3 mb-5 bg-body rounded">
  <div class="p-2 bg-light border">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <p class="card-text">${description}</p>
    </div>
    </div>
    </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div> */}